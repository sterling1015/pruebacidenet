package com.cidenet.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cidenet.api.entity.AreaServicio;
import com.cidenet.api.entity.Empleado;
import com.cidenet.api.entity.PaisEmpleo;
import com.cidenet.api.entity.TipoIdentificacion;
import com.cidenet.api.repository.AreasServicioRepository;
import com.cidenet.api.repository.EmpleadoRepository;
import com.cidenet.api.repository.PaisesEmpleoRepository;
import com.cidenet.api.repository.TiposIdentificacionRepository;

@Service
@Transactional
public class EmpleadoService {

	@Autowired
	private EmpleadoRepository empleadoRepository;
	
	@Autowired
	private TiposIdentificacionRepository tiposIdentificacionRepository;
	
	@Autowired
	private AreasServicioRepository areasServicioRepository;
	
	@Autowired
	private PaisesEmpleoRepository paisesEmpleoRepository;

	public List<Empleado> listaEmpleados() {
		return empleadoRepository.findAll();
	}

	public boolean existsByNumeroIdentificacion(String numeroIdentificacion) {
		return empleadoRepository.existsByNumeroIdentificacion(numeroIdentificacion);
	}
	
	public boolean existsByTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
		return empleadoRepository.existsByTipoIdentificacion(tipoIdentificacion);
		
	}

	public void save(Empleado empleadoNuevo) {
		empleadoRepository.save(empleadoNuevo);
		
	}
	
	public boolean existsByCorreoElectronico(String correoElectronico) {
		return empleadoRepository.existsByCorreoElectronico(correoElectronico);
		
	}

	public int getMaxId() {
		return empleadoRepository.getIdMax();
	}

	public List<TipoIdentificacion> getListTipoIdentificacion() {
		return tiposIdentificacionRepository.findAll();
	}

	public List<AreaServicio> getListAreasDeServicio() {
		return areasServicioRepository.findAll();
	}

	public boolean existsById(int id) {
		return empleadoRepository.existsById(id);
	}

	public void delete(int id) {
		empleadoRepository.deleteById(id);
		
	}

	public Optional<Empleado> getByNumeroIdentificacion(String numeroIdentificacion) {
		return empleadoRepository.findByNumeroIdentificacion(numeroIdentificacion);
	}
	
	public Optional<Empleado> getByCorreoElectronico(String correoElectronico) {
		return empleadoRepository.findByCorreoElectronico(correoElectronico);
	}

	public Optional<Empleado> findById(int id) {
		return empleadoRepository.findById(id);
	}

	public Optional<Empleado> getOne(int id) {
		 return empleadoRepository.findById(id);
	}

	public Optional<Empleado> encontrarEmpleadoAux(int id, String numeroDoc) {
		return empleadoRepository.encontrarEmpleadoAux(id,numeroDoc);
	}

	public Optional<Empleado> getByTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
		return empleadoRepository.findByTipoIdentificacion(tipoIdentificacion);
	}

	public int getContadorTabla() {
		
		return empleadoRepository.getContadorTabla();
		
	}

	public void updateContTable(int contadorTabla) {
		empleadoRepository.updateContTable(contadorTabla);
		
	}

	public List<PaisEmpleo> getListPaisEmpleo() {
		return paisesEmpleoRepository.findAll();
	}
}
