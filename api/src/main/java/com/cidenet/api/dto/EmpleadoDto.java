package com.cidenet.api.dto;

import java.util.Date;

import com.cidenet.api.entity.AreaServicio;
import com.cidenet.api.entity.PaisEmpleo;
import com.cidenet.api.entity.TipoIdentificacion;

public class EmpleadoDto {

	private int id;
	private String primerApellido;
	private String segundoApellido;
	private String primerNombre;
	private String otrosNombres;
	private PaisEmpleo paisEmpleo;
	private TipoIdentificacion tipoIdentificacion;
	private String numeroIdentificacion;
	private String correoElectronico;
	private Date fechaIngreso;
	private AreaServicio areaServicio;
	private Boolean estado;
	private Date fechaRegistro;
	private Date fechaEdicion;

	public EmpleadoDto() {

	}

	public EmpleadoDto(int id, String primerApellido, String segundoApellido, String primerNombre, String otrosNombres,
			PaisEmpleo paisEmpleo, TipoIdentificacion tipoIdentificacion, String numeroIdentificacion,
			String correoElectronico, Date fechaIngreso, AreaServicio areaServicio, Boolean estado, Date fechaRegistro,
			Date fechaEdicion) {

		this.id = id;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.primerNombre = primerNombre;
		this.otrosNombres = otrosNombres;
		this.paisEmpleo = paisEmpleo;
		this.tipoIdentificacion = tipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
		this.correoElectronico = correoElectronico;
		this.fechaIngreso = fechaIngreso;
		this.areaServicio = areaServicio;
		this.estado = estado;
		this.fechaRegistro = fechaRegistro;
		this.fechaEdicion = fechaEdicion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getOtrosNombres() {
		return otrosNombres;
	}

	public void setOtrosNombres(String otrosNombres) {
		this.otrosNombres = otrosNombres;
	}

	public PaisEmpleo getPaisEmpleo() {
		return paisEmpleo;
	}

	public void setPaisEmpleo(PaisEmpleo paisEmpleo) {
		this.paisEmpleo = paisEmpleo;
	}

	public TipoIdentificacion getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public AreaServicio getAreaServicio() {
		return areaServicio;
	}

	public void setAreaServicio(AreaServicio areaServicio) {
		this.areaServicio = areaServicio;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaEdicion() {
		return fechaEdicion;
	}

	public void setFechaEdicion(Date fechaEdicion) {
		this.fechaEdicion = fechaEdicion;
	}

}
