package com.cidenet.api.utils;

public class Constantes {
	public static final String MSJ_NUMERO_DOCUMENTO_EXISTE = "El numero de documento ya está registrado";
	public static final String MSJ_NUMERO_DOCUMENTO_TIPOID_EXISTE = "El numero de documento ya está registrado con este tipo de documento";
	public static final String MSJ_EMPLEADO_CREADO = "El empleado fue registrado";
	public static final String MSJ_DOMINIO_CO = "@cidenet.com.co";
	public static final String MSJ_DOMINIO_US = "@cidenet.com.us";
	public static final String MSJ_PUNTO = ".";
	public static final String MSJ_PAIS_COLOMBIA = "Colombia";
	public static final String MSJ_PAIS_USA = "Estados Unidos";
	public static final int MSJ_UNO = 1;
	public static final String MSJ_ESPACIO = " ";
	public static final String MSJ_VACIO = "";
	public static final String MSJ_REGISTRO_NO_EXISTE = "El empleado no existe";
	public static final String MSJ_REGISTRO_ELIMINADO = "El registro fue eliminado";
	public static final Boolean MSJ_ESTADO_ACTIVO = true;
	public static final String MSJ_REGISTRO_ACTUALIZADO = "Registro actualizado";
	
	//LOGS
	public static final String MSJ_LOG_LISTEMPL_CONTROLLER = "Inicio de metodo listaEmpleados en controlador";
	public static final String MSJ_LOG_DOM_MODIFI = "Retorno del dominio modificado";
	public static final String MSJ_LOG_CORREO_MODIFI = "Retorno del correo modificado";
	public static final String MSJ_LOG_LIST_TIPOID = "Retorno de la lista de tipos de identifiación";
	public static final String MSJ_LOG_LIST_AREASERVICIO = "Retorno de la lista de areas de servicio";
	public static final String MSJ_LOG_LIST_PAISEMPLEO = "Retorno de la lista de paises de empleo";
	
}
