package com.cidenet.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cidenet.api.entity.TipoIdentificacion;
@Repository
public interface TiposIdentificacionRepository extends JpaRepository<TipoIdentificacion, Integer>{
	
	

}
