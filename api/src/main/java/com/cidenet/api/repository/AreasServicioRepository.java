package com.cidenet.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cidenet.api.entity.AreaServicio;

@Repository
public interface AreasServicioRepository extends JpaRepository<AreaServicio, Integer>{

}
