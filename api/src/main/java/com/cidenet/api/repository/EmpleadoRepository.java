package com.cidenet.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.cidenet.api.entity.Empleado;
import com.cidenet.api.entity.TipoIdentificacion;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Integer>{
	
	Optional<Empleado> numeroIdentificacion(String numeroIdentificacion);
	
	boolean existsByNumeroIdentificacion(String numeroIdentificacion);
	
	boolean existsByTipoIdentificacion(TipoIdentificacion tipoIdentificacion);
	
	boolean existsByCorreoElectronico(String correoElectronico);
	
	@Query(value = "SELECT max(id) from Empleado")
	public int getIdMax();

	Optional<Empleado> findByNumeroIdentificacion(String numeroIdentificacion);

	@Query(value = "SELECT e.tipoIdentificacion, e.numeroIdentificacion from Empleado e where e.tipoIdentificacion = ?1 and e.numeroIdentificacion = ?2")
	Optional<Empleado> encontrarEmpleadoAux(int id, String numeroDoc);

	Optional<Empleado> findByTipoIdentificacion(TipoIdentificacion tipoIdentificacion);

	Optional<Empleado> findByCorreoElectronico(String correoElectronico);

	@Query(value = "SELECT t.contador from Configuracion t")
	int getContadorTabla();

	@Modifying
	@Query(value = "UPDATE Configuracion SET contador = ?1")
	int updateContTable(int contadorTabla); 
	
	

}
