package com.cidenet.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cidenet.api.entity.PaisEmpleo;

@Repository
public interface PaisesEmpleoRepository extends JpaRepository<PaisEmpleo, Integer>{

}
