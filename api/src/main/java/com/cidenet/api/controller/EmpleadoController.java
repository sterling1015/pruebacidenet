package com.cidenet.api.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.api.dto.EmpleadoDto;
import com.cidenet.api.entity.AreaServicio;
import com.cidenet.api.entity.Empleado;
import com.cidenet.api.entity.PaisEmpleo;
import com.cidenet.api.entity.TipoIdentificacion;
import com.cidenet.api.service.EmpleadoService;
import com.cidenet.api.utils.Constantes;
import com.cidenet.api.utils.Mensaje;
import com.cidenet.api.utils.TipoLog;
import com.cidenet.api.utils.UtilesLog;

@RestController
@RequestMapping("/empleado")
@CrossOrigin(origins = "http://localhost:4200", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,
		RequestMethod.PUT })
public class EmpleadoController {

	@Autowired
	private EmpleadoService empleadoService;

	private UtilesLog utileslog;

	/**
	 * Metodo que obtiene toda la lista de empleados que se encuentran registrados
	 * 
	 * @return List<Empleado> lista de empleados registrados
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@GetMapping("/lista")
	public ResponseEntity<List<Empleado>> listaEmpleados() {
		List<Empleado> list = empleadoService.listaEmpleados();

		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_LOG_LISTEMPL_CONTROLLER);
		return new ResponseEntity(list, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/detail/{id}")
	public ResponseEntity<Empleado> getById(@PathVariable("id") int id) {
		if (!empleadoService.existsById(id))
			return new ResponseEntity(new Mensaje(Constantes.MSJ_REGISTRO_NO_EXISTE), HttpStatus.NOT_FOUND);
		Empleado producto = empleadoService.getOne(id).get();
		return new ResponseEntity(producto, HttpStatus.OK);
	}

	/**
	 * Metodo que se encargar de registrar un nuevo empleado al sistema
	 * 
	 * @param empleadoDto
	 * @return ResponseEntity
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@PostMapping("/create")
	public ResponseEntity<?> create(@RequestBody EmpleadoDto empleadoDto) {

		String dominio = Constantes.MSJ_VACIO;
		String correoFinal = Constantes.MSJ_VACIO;
		String correoAdaptado = Constantes.MSJ_VACIO;

		if (empleadoService.existsByNumeroIdentificacion(empleadoDto.getNumeroIdentificacion())
				&& empleadoService.existsByTipoIdentificacion(empleadoDto.getTipoIdentificacion())) {
			utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_NUMERO_DOCUMENTO_TIPOID_EXISTE);

			return new ResponseEntity(new Mensaje(Constantes.MSJ_NUMERO_DOCUMENTO_TIPOID_EXISTE),
					HttpStatus.BAD_REQUEST);
		}

		dominio = obtenerDominioPais(empleadoDto);

		correoFinal = empleadoDto.getPrimerNombre().toLowerCase() + Constantes.MSJ_PUNTO
				+ empleadoDto.getPrimerApellido().toLowerCase() + dominio;

		correoAdaptado = convertirCorreo(correoFinal, empleadoDto, dominio);

		empleadoDto.setEstado(Constantes.MSJ_ESTADO_ACTIVO);
		empleadoDto.setFechaRegistro(new Date());
		empleadoDto.setFechaEdicion(null);
		empleadoDto.setCorreoElectronico(correoAdaptado);

		String otrosNombresAux = empleadoDto.getOtrosNombres();
		otrosNombresAux = (otrosNombresAux == null || otrosNombresAux == "") ? "" : otrosNombresAux;

		Empleado empleadoNuevo = new Empleado(0, empleadoDto.getPrimerApellido(), empleadoDto.getSegundoApellido(),
				empleadoDto.getPrimerNombre(), otrosNombresAux, empleadoDto.getPaisEmpleo(),
				empleadoDto.getTipoIdentificacion(), empleadoDto.getNumeroIdentificacion(),
				empleadoDto.getCorreoElectronico(), empleadoDto.getFechaIngreso(), empleadoDto.getAreaServicio(),
				empleadoDto.getEstado(), empleadoDto.getFechaRegistro(), empleadoDto.getFechaEdicion());

		empleadoService.save(empleadoNuevo);
		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_EMPLEADO_CREADO);
		return new ResponseEntity(new Mensaje(Constantes.MSJ_EMPLEADO_CREADO), HttpStatus.OK);

	}

	/**
	 * Metodo encargado de actualizar el registro en base de datos
	 * 
	 * @param id
	 * @param empleadoDto
	 * @return ResponseEntity
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@PutMapping("/update/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody EmpleadoDto empleadoDto) {

		String correoAdaptadoUpdate = Constantes.MSJ_VACIO;
		String correoFinalUpdate = Constantes.MSJ_VACIO;
		String dominioUpdate = Constantes.MSJ_VACIO;

		if (!empleadoService.existsById(id)) {
			utileslog.registrarInfo(this.getClass(), TipoLog.ERROR, Constantes.MSJ_REGISTRO_NO_EXISTE);
			return new ResponseEntity(new Mensaje(Constantes.MSJ_REGISTRO_NO_EXISTE), HttpStatus.NOT_FOUND);
		}

		Empleado empleado = empleadoService.findById(id).get();

		empleado.setPrimerApellido(empleadoDto.getPrimerApellido());
		empleado.setSegundoApellido(empleadoDto.getSegundoApellido());
		empleado.setPrimerNombre(empleadoDto.getPrimerNombre());
		empleado.setOtrosNombres(empleadoDto.getOtrosNombres());
		empleado.setPaisEmpleo(empleadoDto.getPaisEmpleo());
		empleado.setTipoIdentificacion(empleadoDto.getTipoIdentificacion());
		empleado.setNumeroIdentificacion(empleadoDto.getNumeroIdentificacion());

		dominioUpdate = obtenerDominioPais(empleadoDto);

		correoFinalUpdate = empleadoDto.getPrimerNombre().toLowerCase() + Constantes.MSJ_PUNTO
				+ empleadoDto.getPrimerApellido().toLowerCase() + dominioUpdate;

		correoAdaptadoUpdate = convertirCorreo(correoFinalUpdate, empleadoDto, dominioUpdate);

		empleado.setCorreoElectronico(correoAdaptadoUpdate);
		empleado.setFechaIngreso(empleadoDto.getFechaIngreso());
		empleado.setAreaServicio(empleadoDto.getAreaServicio());
		empleado.setEstado(Constantes.MSJ_ESTADO_ACTIVO);
		empleado.setFechaEdicion(new Date());
		empleadoService.save(empleado);
		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_REGISTRO_ACTUALIZADO);
		return new ResponseEntity(new Mensaje(Constantes.MSJ_REGISTRO_ACTUALIZADO), HttpStatus.OK);
	}

	/**
	 * Metodo que se encarga de obtener el dominio del correo según el pais
	 * 
	 * @param empleadoDto
	 * @return dominio del correo modificado
	 */
	@SuppressWarnings("static-access")
	private String obtenerDominioPais(EmpleadoDto empleadoDto) {
		String dominioUpdate = Constantes.MSJ_VACIO;
		if (empleadoDto.getPaisEmpleo().equals(Constantes.MSJ_PAIS_COLOMBIA)) {
			dominioUpdate = Constantes.MSJ_DOMINIO_CO;
		} else {
			dominioUpdate = Constantes.MSJ_DOMINIO_US;
		}
		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_LOG_DOM_MODIFI);
		return dominioUpdate;
	}

	/**
	 * Metodo que adapta o convierte el correo para su respectiva inserción
	 * 
	 * @param correoFinal
	 * @param empleadoDto
	 * @param dominio
	 * @param contadorEmail
	 * @return correoFinal
	 */
	@SuppressWarnings("static-access")
	private String convertirCorreo(String correoFinal, EmpleadoDto empleadoDto, String dominio) {
		int contadorTabla = 0;

		correoFinal = correoFinal.replace(Constantes.MSJ_ESPACIO, Constantes.MSJ_VACIO);

		if (empleadoService.existsByCorreoElectronico(correoFinal)) {

			contadorTabla = empleadoService.getContadorTabla();
			contadorTabla = contadorTabla + Constantes.MSJ_UNO;

			correoFinal = empleadoDto.getPrimerNombre().toLowerCase().replace(Constantes.MSJ_ESPACIO,
					Constantes.MSJ_VACIO) + Constantes.MSJ_PUNTO
					+ empleadoDto.getPrimerApellido().toLowerCase().replace(Constantes.MSJ_ESPACIO,
							Constantes.MSJ_VACIO)
					+ Constantes.MSJ_PUNTO + contadorTabla + dominio;

			empleadoService.updateContTable(contadorTabla);

		} else {
			correoFinal = empleadoDto.getPrimerNombre().toLowerCase().replace(Constantes.MSJ_ESPACIO,
					Constantes.MSJ_VACIO) + Constantes.MSJ_PUNTO
					+ empleadoDto.getPrimerApellido().toLowerCase().replace(Constantes.MSJ_ESPACIO,
							Constantes.MSJ_VACIO)
					+ dominio;
		}
		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_LOG_CORREO_MODIFI);
		return correoFinal;

	}

	/**
	 * Metodo que obtiene la lista de los tipos de identificación
	 * 
	 * @return ResponseEntity lista de los tipos de identificación
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@GetMapping("/tiposIdentificacion")
	public ResponseEntity<List<TipoIdentificacion>> tiposIdentificacion() {
		List<TipoIdentificacion> listTipoIdentificacion = empleadoService.getListTipoIdentificacion();
		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_LOG_LIST_TIPOID);
		return new ResponseEntity(listTipoIdentificacion, HttpStatus.OK);
	}

	/**
	 * Metodo que obtiene la lista de las areas de servicio
	 * 
	 * @return ResponseEntity lista de areas de servicio
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@GetMapping("/areasDeServicio")
	public ResponseEntity<List<AreaServicio>> areasDeServicio() {
		List<AreaServicio> listAreasDeServicio = empleadoService.getListAreasDeServicio();
		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_LOG_LIST_AREASERVICIO);
		return new ResponseEntity(listAreasDeServicio, HttpStatus.OK);
	}
	
	/**
	 * Metodo que obtiene la lista de los tipos de pais
	 * 
	 * @return ResponseEntity lista de paises
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@GetMapping("/tiposPaisEmpleo")
	public ResponseEntity<List<PaisEmpleo>> tiposPaisEmpleo() {
		List<PaisEmpleo> listPaisesEmpleo = empleadoService.getListPaisEmpleo();
		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_LOG_LIST_PAISEMPLEO);
		return new ResponseEntity(listPaisesEmpleo, HttpStatus.OK);
	}	

	/**
	 * Metodo que se encarga de eliminar el empleado
	 * 
	 * @param id
	 * @return ResponseEntity
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id) {
		if (!empleadoService.existsById(id)) {
			utileslog.registrarInfo(this.getClass(), TipoLog.ERROR, Constantes.MSJ_REGISTRO_NO_EXISTE);
			return new ResponseEntity(new Mensaje(Constantes.MSJ_REGISTRO_NO_EXISTE), HttpStatus.NOT_FOUND);
		}
		empleadoService.delete(id);
		utileslog.registrarInfo(this.getClass(), TipoLog.INFO, Constantes.MSJ_REGISTRO_ELIMINADO);
		return new ResponseEntity(new Mensaje(Constantes.MSJ_REGISTRO_ELIMINADO), HttpStatus.OK);
	}

}
