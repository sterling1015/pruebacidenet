INSERT INTO configuracion (contador) values (0);

--INSERT TABLA tipoIdentificacion
INSERT INTO tipo_identificacion (nombre) values ('Cédula de Ciudadanía');
INSERT INTO tipo_identificacion (nombre) values ('Cédula de Extranjería');
INSERT INTO tipo_identificacion (nombre) values ('Pasaporte');
INSERT INTO tipo_identificacion (nombre) values ('Permiso Especial');
--INSERT TABLA areaServicio
INSERT INTO area_servicio (nombre) values ('Administración');
INSERT INTO area_servicio (nombre) values ('Financiera');
INSERT INTO area_servicio (nombre) values ('Compras');
INSERT INTO area_servicio (nombre) values ('Infraestructura');
INSERT INTO area_servicio (nombre) values ('Operación');

INSERT INTO pais_empleo (nombre_pais) values ('Colombia');
INSERT INTO pais_empleo (nombre_pais) values ('Estados unidos');

--INSERT TABLA empleado
INSERT INTO empleado (primer_apellido,segundo_apellido,primer_nombre,otros_nombres,pais_empleo,tipo_identificacion,numero_identificacion,correo_electronico,fecha_ingreso,area_servicio,estado) values ('STERLING','VALENCIA','STEVEN','',1,1,'1225091401','steven.sterling@cidenet.com.co',sysdate(),1,true);