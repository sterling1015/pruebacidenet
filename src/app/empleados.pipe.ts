import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class EmpleadosPipe implements PipeTransform {
  estadoString:any;
  transform(value: any, arg: any): any {
    
    if (arg === '' || arg.length < 3) return value;
    const resultSearch = [];
    for (const empleado of value) {
      if(empleado.estado === true){
        this.estadoString = 'Activo';

      }

      if (empleado.primerNombre.toLowerCase().indexOf(arg.toLowerCase()) > -1
        || empleado.otrosNombres.toLowerCase().indexOf(arg.toLowerCase()) > -1
        || empleado.primerApellido.toLowerCase().indexOf(arg.toLowerCase()) > -1
        || empleado.segundoApellido.toLowerCase().indexOf(arg.toLowerCase()) > -1
        || empleado.numeroIdentificacion.indexOf(arg) > -1
        || empleado.paisEmpleo.nombrePais.toLowerCase().indexOf(arg.toLowerCase()) > -1
        || empleado.correoElectronico.toLowerCase().indexOf(arg.toLowerCase()) > -1
        || empleado.tipoIdentificacion.nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1
        || this.estadoString.toLowerCase().indexOf(arg.toLowerCase()) > -1
        ) {
        resultSearch.push(empleado);
      };

    };
    return resultSearch;
  }

}
