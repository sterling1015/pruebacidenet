import { Component, OnInit } from '@angular/core';
import { Empleados } from "../../../models/empleados";
import { EmpleadosService } from "../empleados.service";
import { ActivatedRoute, Router } from '@angular/router';
import Swal from "sweetalert2";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TipoIdentificacion } from "../../../models/tipoIdentificacion";
import { AreaServicio } from "../../../models/areaServicio";
import { PaisEmpleo } from "../../../models/paisEmpleo";

@Component({
  selector: 'app-form-edit',
  templateUrl: './form-edit.component.html',
  styleUrls: ['./form-edit.component.css']
})
export class FormEditComponent implements OnInit {

  /**Declaracion de variables */
  titulo: string = "Cidenet S.A.S";
  paisesEmpleoId: any;
  tiposIdenfitifacionId: any;
  areasServicioId: any;
  submitted = false;
  editEmpForm: FormGroup;
  empleados: Empleados = null;
  tiposIdentificacion: TipoIdentificacion[];
  areaServicios: AreaServicio[];
  paisesEmpleo: PaisEmpleo[];
  valorFechaIngresoEdit:any;

  constructor(
    private empleadosService: EmpleadosService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getTiposIdentificacion();
    this.getTiposAreaServicio();
    this.getTiposPaisEmpleo();

    /** validar regex de campos */
    this.editEmpForm = this.formBuilder.group({
      primerApellido: ['', [Validators.required, Validators.maxLength(20), Validators.pattern(/^([A-Z\s]){0,20}$/) ]],
      segundoApellido: ['', [Validators.required, Validators.maxLength(20), Validators.pattern(/^([A-Z\s]){0,20}$/)]],
      primerNombre: ['', [Validators.required, Validators.maxLength(20), Validators.pattern(/^([A-Z\s]){0,20}$/)]],
      otrosNombres: ['', [Validators.maxLength(50),Validators.pattern(/^([A-Z\s]){0,20}$/)]],
      numeroIdentificacion: ['', [Validators.pattern(/^([A-Za-z-\d])*$/), Validators.required]],
      fechaIngreso:[],
      tipoPais:[],
      areaServicio:[],
      tiposAreasServicio:[],
      tipoIdentificacion:[]

    })
    /** Busca el id del registro a editar */
    const id = this.activatedRoute.snapshot.params.id;
    this.empleadosService.detail(id).subscribe(
      data => {
        this.empleados = data;
      }, err => {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.mensaje
        })

      }
    );

  }
  /**Lista los tipos de identificacion disponibles*/
  getTiposIdentificacion() {
    this.empleadosService.getTiposIdentificacion().subscribe(
      (result) => {
        this.tiposIdentificacion = result;
      }
    );
  }
  /**Lista las areas de servicio*/
  getTiposAreaServicio() {
    this.empleadosService.getTiposAreaServicio().subscribe(
      (result) => {
        this.areaServicios = result;
      }
    );
  }

  /**Lista los paises registrados */
  getTiposPaisEmpleo() {
    this.empleadosService.getTiposPaisEmpleo().subscribe(
      (result) => {
        this.paisesEmpleo = result;
      }
    );
  }

  /** Obtiene solo las fechas anteriores y de hoy*/
  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }
  /** Función encargada de editar el registro */
  onSubmitEditar(): void {
    this.submitted = true;

    if (this.editEmpForm.invalid) {
      return;
    }

    this.paisesEmpleoId = document.getElementById('paisesEmpleo');
    this.tiposIdenfitifacionId = document.getElementById('tiposIdentificacion');
    this.areasServicioId = document.getElementById('tiposAreasServicio');

    const id = this.activatedRoute.snapshot.params.id;
    this.empleadosService.update(id, this.empleados)
      .subscribe(empleados => {
        this.router.navigate([''])
        Swal.fire('Actualización', `Empleado actualizado con éxito!`, 'success')
      }, err => {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.mensaje
        })

      }
      );
  }  
  get f() { return this.editEmpForm.controls; }

  /** Compara los tipos de identificacion por el id para mostrar en el fomulario de editar */
  compararTipoId(o1:TipoIdentificacion,o2:TipoIdentificacion){
    return o1 === null || o2 === null? false: o1.id === o2.id;
  }
  /** Compara los tipos de area por el id para mostrar en el fomulario de editar */ 
  compararTipoArea(o1: AreaServicio,o2:AreaServicio){
    return o1 === null || o2 === null? false: o1.idArea === o2.idArea;

  }
  /** Compara los tipos de pais por el id para mostrar en el formulario de editar */
  compararTipoPais(o1: PaisEmpleo,o2:PaisEmpleo){
    return o1 === null || o2 === null? false: o1.idPais === o2.idPais;
  }

}
