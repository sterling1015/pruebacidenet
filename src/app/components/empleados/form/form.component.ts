import { Component, OnInit } from '@angular/core';
import { Empleados } from "../../../models/empleados";
import { EmpleadosService } from "../empleados.service";
import { Router, ActivatedRoute } from '@angular/router'
import Swal from 'sweetalert2';
import { TipoIdentificacion } from "../../../models/tipoIdentificacion";
import { AreaServicio } from "../../../models/areaServicio";
import { PaisEmpleo } from "../../../models/paisEmpleo";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  /** Declaración de variables */
  registerEmpForm: FormGroup;
  submitted = false;
  public empleados: Empleados = new Empleados();
  public titulo: string = "Crear Empleado"
  tiposIdentificacion: TipoIdentificacion[];
  areaServicios: AreaServicio[];
  paisesEmpleoId: any;
  tiposIdenfitifacionId: any; y
  areasServicioId: any;
  paisesEmpleo: PaisEmpleo[];

  constructor(private empleadosService: EmpleadosService, private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {

    /** Validaciones de campos de formulario */
    this.registerEmpForm = this.formBuilder.group({
      primerApellido: ['', [Validators.required, Validators.maxLength(20), Validators.pattern(/^([A-Z\s]){0,20}$/)]],
      segundoApellido: ['', [Validators.required, Validators.maxLength(20), Validators.pattern(/^([A-Z\s]){0,20}$/)]],
      primerNombre: ['', [Validators.required, Validators.maxLength(20), Validators.pattern(/^([A-Z\s]){0,20}$/)]],
      otrosNombres: ['', [Validators.maxLength(50), Validators.pattern(/^([A-Z\s]){0,20}$/)]],
      numeroIdentificacion: ['', [Validators.required,Validators.pattern(/^([A-Za-z-\d])*$/)]],
      fechaIngreso: [],
      tipoPais: [],
      tipoIdentificacion: [],
      tipoAreasServicio: []

    })

    /** Funciones que se ejecutan cuando se carga el sistema */
    this.getTiposIdentificacion();
    this.getTiposAreaServicio();
    this.getTiposPaisEmpleo();

  }
  /** Obtiene los dias anteriores a la fecha actual para el calendario */
  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }
  /** Obtiene la lista de los tipos de identificacion */
  getTiposIdentificacion() {
    this.empleadosService.getTiposIdentificacion().subscribe(
      (result) => {
        this.tiposIdentificacion = result;
      }
    );
  }
  /** Obtiene la lista de las areas de servicio */
  getTiposAreaServicio() {
    this.empleadosService.getTiposAreaServicio().subscribe(
      (result) => {
        this.areaServicios = result;
      }
    );
  }
  /** Obtiene la lista de los paises de empleo */
  getTiposPaisEmpleo() {
    this.empleadosService.getTiposPaisEmpleo().subscribe(
      (result) => {
        this.paisesEmpleo = result;
        
      }
    );
  }
  /** funcion para controlar el flujo de errores */
  get f() { return this.registerEmpForm.controls; }

  /** Función que se encarga de hacer el envio de los datos para la insersión */
  onSubmit(): void {

    this.submitted = true;

    if (this.registerEmpForm.invalid) {
      return;
    }

    this.paisesEmpleoId = document.getElementById('tipoPais');
    this.tiposIdenfitifacionId = document.getElementById('tiposIdentificacion');
    this.areasServicioId = document.getElementById('tiposAreasServicio');

    this.empleados.tipoIdentificacion = {
      id: this.tiposIdenfitifacionId.value,
      nombre: ""
    };

    this.empleados.areaServicio = {
      idArea: this.areasServicioId.value,
      nombre: ""
    };

    this.empleados.paisEmpleo = {
      idPais: this.paisesEmpleoId.value,
      nombrePais: ""
    };
    debugger;
    if (this.empleados.paisEmpleo.idPais === 0
      || this.empleados.paisEmpleo.idPais === null
      || this.empleados.paisEmpleo.idPais.toString() === "") {

      this.empleados.paisEmpleo.idPais = 1;

    }

    /** Si se manda vacio el tipo de identificador se le asigna cédula de ciudadania por defecto */
    if (this.empleados.tipoIdentificacion.id === null || this.empleados.tipoIdentificacion.id.toString() === "") {
      this.empleados.tipoIdentificacion.id = 1;
    }

    /** Si se manda vacio o nullo el area se le asigna por defecto el area de operación */
    if (this.empleados.areaServicio.idArea === null || this.empleados.areaServicio.idArea.toString() === "") {
      this.empleados.areaServicio.idArea = 4;

    }
    
    this.empleadosService.create(this.empleados)
      .subscribe(empleados => {
        this.router.navigate([''])
        Swal.fire('Nuevo cliente', `Cliente creado con éxito!`, 'success')
      }, err => {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: err.error.mensaje
        })

      }
      );
  }


}
