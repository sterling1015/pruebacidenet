import { Component, OnInit } from '@angular/core';
import { EmpleadosService } from "./empleados.service";
import { Empleados } from "../../models/empleados";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {

  /** Declaración de variables */ 
  nombreCompletoEmpleado: any;
  empleados: Empleados[];
  estado: any;
  public page: number;
  filterPost = '';
  constructor(private empleadoService: EmpleadosService) { }

  ngOnInit(): void {
    this.getListaEmpleados();

  }
  /** Función que obtiene la lista de todos los empleados registrados */
  getListaEmpleados() {
    this.empleadoService.getEmpleados().subscribe(
      (result) => {
        this.empleados = result;
      }
    );
    
  }
  /** Función que elimina un empleado especifico por id */
  delete(empleado: Empleados): void {

    Swal.fire({
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar al cliente ${empleado.primerNombre} ${empleado.primerApellido}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',      
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!'
    }).then((result) => {
      if(result.isConfirmed){
        this.empleadoService.delete(empleado.id).subscribe(
          response => {
            this.empleados = this.empleados.filter(cli => cli !== empleado)
            Swal.fire(
              'Cliente Eliminado!',
              `Cliente ${empleado.primerNombre} eliminado con éxito.`,
              'success'
            )
          }
        )
      }
    });
  }
}
