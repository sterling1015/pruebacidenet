import { Injectable } from '@angular/core';
import { Empleados } from "../../models/empleados";
import { TipoIdentificacion } from "../../models/tipoIdentificacion";
import { PaisEmpleo } from "../../models/paisEmpleo";
import { AreaServicio } from "../../models/areaServicio";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  private urlEndPoint: string = 'http://localhost:8080/empleado';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})


  constructor(private http: HttpClient) { }


  getEmpleados(): Observable<Empleados[]> {
    return this.http.get(this.urlEndPoint+'/lista').pipe(
      map(response => response as Empleados[])
    );
  }

  detail(id: number): Observable<Empleados> {
    return this.http.get<Empleados>(this.urlEndPoint + `/detail/${id}`);
  }  

  getTiposIdentificacion(): Observable<TipoIdentificacion[]> {
    return this.http.get(this.urlEndPoint+'/tiposIdentificacion').pipe(
      map(response => response as TipoIdentificacion[])
    );
  }

  getTiposAreaServicio(): Observable<AreaServicio[]> {
    return this.http.get(this.urlEndPoint+'/areasDeServicio').pipe(
      map(response => response as AreaServicio[])
    );
  }

  getTiposPaisEmpleo(): Observable<PaisEmpleo[]> {
    return this.http.get(this.urlEndPoint+'/tiposPaisEmpleo').pipe(
      map(response => response as PaisEmpleo[])
    );
  }

  delete(id: number): Observable<Empleados>{
    return this.http.delete<Empleados>(`${this.urlEndPoint+'/delete'}/${id}`, {headers: this.httpHeaders})
  }  

  create(empleados: Empleados) : Observable<Empleados> {
    return this.http.post<Empleados>(this.urlEndPoint+'/create', empleados, {headers: this.httpHeaders})
  }

  update(id: number, empleados: Empleados): Observable<any> {
    return this.http.put<any>(this.urlEndPoint + `/update/${id}`, empleados);
  }

}
