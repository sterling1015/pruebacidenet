import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpleadosComponent } from "./components/empleados/empleados.component";
import { FormComponent } from "./components/empleados/form/form.component";
import { FormEditComponent } from "./components/empleados/form-edit/form-edit.component";

const routes: Routes = [
  {path: '', component: EmpleadosComponent},
  {path: 'addEmpleados', component: FormComponent},
  {path: 'updateEmpleados/:id', component: FormEditComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
  

];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
