import { TipoIdentificacion } from "./tipoIdentificacion";
import { AreaServicio } from "./areaServicio";
import { PaisEmpleo } from "./paisEmpleo";
export class Empleados {

    id:number;
    primerApellido:String;
	segundoApellido:String;
	primerNombre:String;
	otrosNombres:String;
    paisEmpleo:PaisEmpleo;
    tipoIdentificacion: TipoIdentificacion;
	numeroIdentificacion: String;
    correoElectronico: String;
    fechaIngreso: Date;    
    areaServicio: AreaServicio;
    estado: Boolean;
    fechaRegistro: Date;
    fechaEdicion: Date;


}